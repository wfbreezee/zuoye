#include<iostream>
#include<iomanip>
using namespace std;
void swap1(int **a,int n){
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<n-i;j++)
		{
			int tmp=a[i][j];
			a[i][j]=a[n-j-1][n-1-i];
			a[n-j-1][n-1-i]=tmp;
		}
	}
}
void swap2(int **a,int n)
{
	for(int i=0;i<n/2;i++)
	{
		int *tmp = a[i];
		a[i]=a[n-1-i];
		a[n-1-i]=tmp;
	}
	//cout<<a[0]<<a[1]<<a[2]<<a[3]<<endl;
}
int main()
{   

	int **a = new int * [4];
	int m[4][4]={{1,2,3,4},{9,10,11,12},{13,9,5,1},{15,11,7,3}};
	for(int i=0;i<4;i++)
	{
		a[i]=m[i];
	}
	//cout<<a[0]<<a[1]<<a[2]<<a[3]<<endl;
	swap1(a,4);
	swap2(a,4);
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			cout<<setw(4)<<a[i][j];
		}
		cout<<endl;
	}
	return 0;
}