#include <stdio.h>  
#include <stdlib.h>  
#include <math.h>  
int isPrime(int x)  
{  
    if(x<=0)  
    {  
        return 0;  
    }  
    else if(x==1 || x==2)  
    {  
        return 1;  
    }  
    else  
    {  
        int i;  
        for(i=2;i<x;i++)  
        {  
            if(x%i==0)  
            {  
                break;  
            }  
        }  
        if(i==x)  
        {  
            return 1;  
        }  
        else  
        {  
            return 0;  
        }  
    }  
}  
  
int main()  
{  
    int count=0;  
    printf("   #############1000以内的所有素数##############\n");  
    for(int i=1;i<1000;i++)  
    {  
        if(isPrime(i))  
        {  
            printf("%6d",i);  
            count++;  
            if(count%5!=0)  
            continue;
                
            printf("\n");  
             
        }  
    }  
    printf("\n");  
    return 0;
}  
