#include<stdio.h>
#include<iostream>
using namespace std; 

void fac1(int x)
{
	int sum=0;
	do
	{
		sum+=x%10;
		x/=10;
	}while(x);
	cout<<sum<<endl;
	
 } 
 
 void fac2(int x)
 {
 	int sum=0;
 	for(;x;x/=10)
 	{
 		sum+=x%10;
	 }
	 cout<<sum<<endl;
 }
 
 void fac3(int x)
 {
 	int sum=0;
 	while(x!=0)
 	{
 		sum+=x%10;
 		x/=10;
	 }
	 cout<<sum<<endl;
 }
 
 void fac4(int x)
 {
 	int sum=0;
 	while(1)
 	{
 		sum+=x%10;
 		x/=10;
 		if(x==0)
 		    break;
	 }
	  cout<<sum<<endl;
 }
 
 int main()
 {
 	int x;
 	cout<<"请输入一个整数："<<endl; 
 	cin>>x;
 	fac1(x);
 	fac2(x);
 	fac3(x);
 	fac4(x);
 	return 0;
 }
