#include<stdio.h> 
int Add1(int k)
{
	int temp,sum=0,x;
	for(temp=k;temp>0;temp--)
	{
		x=temp;
		for(;x>0;x=x/10)
			sum+=x%10;
	}
	return sum;
}
int Add2(int k)
{
	int temp,sum=0,x;
	temp=k;
	while(temp>0)
	{
		x=temp;
		while(x>0)
		{
			sum+=x%10;
			x=x/10;
		}
		temp--;
	}
	return sum;
}
int Add3(int k)
{
	int temp,sum=0,x;
	temp=k;
	do
	{
		x=temp;
		do
		{
			sum+=x%10;
			x=x/10;
		}while(x>0);
		temp--;
	}while(temp>0);
	return sum;
}
int Add4(int k)
{
	int temp,sum=0,x;
	temp=k;
	while(1)
	{
		if(temp<=0)
			break;
		else
		{
			x=temp;
			while(1)
			{
				if(x<=0)
					break;
				else
				{	
					sum+=x%10;
					x=x/10;
				}
			}
			temp--;
		}
		
	}
	return sum;
}
int main()
{
	int sum,x;
	printf("请输入要计算得值；");
	scanf("%d",&x);
	sum=Add1(x);
	printf("sum=%d\n",sum);
	sum=Add2(x);
	printf("sum=%d\n",sum);
	sum=Add3(x);
	printf("sum=%d\n",sum);
	sum=Add4(x);
	printf("sum=%d\n",sum);
	return 0;
}
