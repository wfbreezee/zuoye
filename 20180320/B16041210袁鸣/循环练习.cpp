#include<iostream>
using namespace std;

int func1(int n)
{
	int s=0;
	for(;n!=0;n=n/10){
	s+=n%10;

	}
	return s;
}

int func2(int n)
{
	int s=0;
	while(n!=0){
		s+=n%10;
		n/=10;
	}

	
	return s;
}

int func3(int n)
{
	int s=0;
	do{
		s+=n%10;
		n/=10;
	}while(n!=0);
	return s;
}

int func4(int n)
{
	int s=0;
	while(1){
		s+=n%10;
		n/=10;
		if(n==0)
		break;	
	}
	return s;
}


int main(){
	int n;
	cin>>n;
	cout<<"for循环结果："<<func1(n)<<endl;
	cout<<"while循环结果："<<func2(n)<<endl;
	cout<<"do while循环结果："<<func3(n)<<endl;
	cout<<"while(1)循环结果："<<func4(n)<<endl;

	
}
