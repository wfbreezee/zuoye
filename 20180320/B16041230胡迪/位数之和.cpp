#include <iostream>
using namespace std;
void function_1(int x){
	int a,b;
	int sum=x%10;
	for (;x!=0;x/=10){
		a=x/10;
		sum+=(a%10);
	}
	cout<<sum<<endl;
}
void function_2(int x){
	int a,b;
	int sum=x%10;
	while(x/10!=0){
		a=x/10;
		sum+=(a%10);
		x/=10;
	} 
	cout<<sum<<endl;
}
void function_3(int x){
	int a,b;
	int sum=x%10;
	do{
		a=x/10;
		sum+=(a%10);
		x/=10;
	}while(x!=0);
	cout<<sum<<endl;
}
void function_4(int x){
	int a,b;
	int sum=x%10;
	while(1){
		if(x/10==0)
			break;
		a=x/10;
		sum+=(a%10);
		x/=10;
	} 
	cout<<sum<<endl;
}
int main (void){
	while(true){
		int x;
		cin>>x;
		function_1(x);
		function_2(x);
		function_3(x);
		function_4(x);
	}
	return 0;
}
