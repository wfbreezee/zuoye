// 四种循环实现数位之和.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include<iostream>
using namespace std;
void sum1(int m)
{
	int sum = 0;
	for (int i = 1;i <= m;i++)
	{
		int a = i;
		for (;a >= 1;)
		{
			sum = sum+a % 10;
			a = a / 10;
		}
	}
	cout << sum<<endl;
}
void sum2(int m)
{
	int sum = 0;
	int i = 1;
	while (i <= m)
	{
		int a = i;
		while (a >= 1)
		{
			sum = sum + a % 10;
			a = a / 10;
		}
		i++;
	}
	cout << sum << endl;
}
void sum3(int m)
{
	int sum = 0;
	int i = 1;
	do
	{
		int a = i;
		do
		{
			sum = sum + a % 10;
			a = a / 10;
		} while (a>0);
		i++;
	} while (i <= m);
	cout << sum << endl;
}
void sum4(int m)
{
	int sum = 0;
	int i = 1;
	while (1)
	{
		int a = i;
		while (1)
		{
			if (a > 0) 
			{
				sum = sum + a % 10;
				a = a / 10;
			}
			else
			{
				break;
			}
		}
		if (i == m)
		{
			break;
		}
		i++;		
	}
	cout << sum << endl;
}
int main()
{
	int a;
	cin >> a;
	sum1(a);
	sum2(a);
	sum3(a);
	sum4(a);
    return 0;
}

