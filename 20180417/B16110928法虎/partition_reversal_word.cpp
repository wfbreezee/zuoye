#include <iostream>

using namespace std;

int isBd(int ch)
{
    if( ch==' '|| ch==',' || ch=='.' || ch=='!' || ch=='?' )
        return 1;
    return 0;
}

void partition_reversal_word(char* s)
{
    int i=0;
    while(s[i]!='\0')
    {
        while(s[i]!='\0' && isBd(s[i]))
        {
            cout<< s[i];
            i++;
        }
        int start=i;
        while(s[i]!='\0' && !isBd(s[i])) i++;
        int end=i-1;

        for(int k=end; k>=start; k--)
            cout << s[k];

    }
}

int main()
{
    char s[100]=" I  like,,,.. C++ programming!";
    partition_reversal_word(s);
    return 0;
}
