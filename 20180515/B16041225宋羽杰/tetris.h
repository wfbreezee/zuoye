#pragma once

#include "resource.h"

//函数声明
void checkComplite();  //查看一行是否能消去  采用从上往下的消法，消去一行后把上面的每行都往下移， 
void drawBlocked(HDC hdc);      //绘制当前已经存在砖块的区域
void DrawBackGround(HDC hdc);       //绘制背景
void outPutBoxInt(int num);     //自定义的弹窗函数  用于调试
void outPutBoxString(TCHAR str[1024]);
void setRandomT();      //随机生成一个方块用作下一次掉落
void init_game();       //初始化
void fillBlock();       //到达底部后填充矩阵
void RotateTeris(BOOL bTeris[4][4]);        //旋转矩阵
void DrawTeris(HDC mdc);    //绘制正在下落的方块
void drawNext(HDC hdc); //绘制下一个将要掉落的方块
void drawScore(HDC hdc);    //绘制分数
void drawCompleteParticle(int line);//绘制某一行
int RandomInt(int _min, int _max);       //获取一个随机int
int CheckValide(int curPosX, int curPosY, BOOL bCurTeris[4][4]);   //给定一个矩阵，查看是否合法
int selectDiffculty(HWND hWnd, int dif);//设置难度
int selectLayOut(HWND hWnd, int layout);//设置布局

										//常量声明
const int BORDER_X = 10;
const int BORDER_Y = 10;
const int SCREEN_LEFT_X = 300 + BORDER_X;
const int SCREEN_Y = 600 + BORDER_Y;
const int SCREEN_RIGHT_X = 180 + BORDER_X * 2;
const int SCREEN_X = SCREEN_LEFT_X + SCREEN_RIGHT_X;  //窗口大小
const BOOL state_teris[][4][4] =
{
	{ { 1,1,1,1 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
{ { 0,1,1,0 },{ 0,1,1,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
{ { 0,1,1,1 },{ 0,0,0,1 },{ 0,0,0,0 },{ 0,0,0,0 } },
{ { 0,1,1,0 },{ 0,0,1,1 },{ 0,0,0,0 },{ 0,0,0,0 } },
{ { 0,1,0,0 },{ 1,1,1,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
{ { 0,1,1,1 },{ 0,1,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
{ { 0,1,1,0 },{ 1,1,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } }
};//7种方块形状的矩阵结构

  //全局变量声明
bool g_hasBlocked[50][50];//被填充的矩阵
RECT rc_left, rc_right, rc_right_top, rc_right_bottom, rc_main;//布局
int g_speed = 300;//实际反应时间
int t_speed = 300;//设置的下降一行时间间隔
BOOL bCurTeris[4][4];//当前方块内容
BOOL bNextCurTeris[4][4];//下一个方块内容
int curPosX, curPosY;//方块整体的坐标
int rd_seed = 1995421;//设置的随机数种子
int tPre = 0, tCur;//当前时间
int GAME_STATE = 0;//游戏状态
int GAME_SCORE = 0;//所得分数
int GAME_DIFF = 1;//难度系数
int NUM_X = 10;//方块可以移动的x轴范围
int NUM_Y = 20;//方块可以移动的y轴范围
int BLOCK_SIZE = 30;//方块大小
