#ifndef SYM_H
#define SYM_H
int ** sm(int n){
	int **m=new int*[n];
	for (int i =0;i<n;i++){
		m[i]=new int [n];
	}
	int v=1;
	for(int j=0;j<n;j++){
		for(int t=j;t<n;t++){
			m[j][t]=m[t][j]=v++;
		}
	}
	return m;
}
#endif