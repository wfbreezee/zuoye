
#include <iostream>
#include<iomanip>
using namespace std;
#define n 4
int main()
{
    int v = 1;
    int a[n][n];
    for( int j = 0; j < n; j++)
    {
        for(int i = j; i < n; i++)
        {
            a[i][j]=v;
            v++;
            a[j][i]=a[i][j];
        }
    }
    for( int i = 0; i < n; i++)
    {
        for( int j = 0; j < n; j++)
        {
            cout<<setw(4)<<a[i][j];
        }
        cout<<endl;
    }
    return 0;
}
