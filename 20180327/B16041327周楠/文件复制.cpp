#include<iostream>
#include<stdio.h>
#include<stdlib.h>
using namespace std;
int main(int argc,char**argv)
{
    if(argc==3)
    {
        char* srcfile=argv[1];
        char* dstfile=argv[2];
        FILE *fp1=fopen(srcfile,"r");
        if(fp1==0)
        {
            cout<<"fail to open fp1";
            return -1;
        }
        FILE *fp2=fopen(dstfile,"w");
        if(fp2==0)
        {
            cout<<"fail to open fp2";
            return -1;
        }
        while(1)
        {
            char temp=fgetc(fp1);
            if( feof(fp1))
            break;
            fputc(temp, fp2);
        }

        fclose(fp1);
        fclose(fp2);
    }
}
