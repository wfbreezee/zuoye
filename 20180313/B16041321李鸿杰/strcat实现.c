#include<stdio.h>
#include<assert.h>

char* strcat(char* str1, char* str2)
{
    assert(str1 != NULL && str2 != NULL);
    int i = 0;
    int j = 0;
    while(str1[i] != '\0')
        i++;
    for(j = 0; str2[j] != '\0'; j++, i++)
        str1[i] = str2[j];
    str1[j] = '\0';
    return str1;
}

int main(void)
{
    char head[100];
    char tail[100];
    memset(head, '\0', sizeof(head));
    memset(tail, '\0', sizeof(tail));
    scanf("%s", head);
    fflush(stdin);
    scanf("%s", tail);
    fflush(stdin);
    strcat(head, tail);
    printf("%s", head);
    printf("%s", tail);
    return 0;
}
