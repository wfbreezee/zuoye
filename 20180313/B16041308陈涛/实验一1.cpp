#include<stdio.h>
#include<stdlib.h>
char* strcat(char* str1, char* str2)
{
	int i=0,j=0;
	int k; 
	char *s;
	int length;
	if(str1==NULL||str2==NULL)
	    return NULL;
	while(str1[i]!='\0')
		i++;
	while(str2[j]!='\0')
		j++;
	length=i+j+1;
	s=(char*)malloc(sizeof(char)*length);
	for(k=0;k<i;k++)
	    s[k]=str1[k];
    for(k=i;k<=length-2;k++)
	    s[k]=str2[k-i];
	s[length-1]='\0';
	return s;
	    
}
int main(){
	char *s1,*s2,*s;
	s1=(char*)malloc(sizeof(char)*20);
	s2=(char*)malloc(sizeof(char)*20);
	printf("（字符串最大长度为20）请输入第一个字符串：");
	gets(s1); 
	printf("请输入第二个字符串：");
	gets(s2); 
	s=strcat(s1,s2);
	printf("链接后的字符串：");
	printf("%s",s);
	free(s1);
	free(s2); 
	return 0;
}
