#include<stdio.h>

int test(int n){  
	int sum = 0;
	for(int i = 1; i <= n; i++){
		int temp = i;  // 复制一下变量 
		while(temp != 0){
			sum += temp % 10; 
			temp /= 10;
		}
	}
	return sum;
}

int main(){
	int x = 12;
	printf("%d\n", test(x));
}
