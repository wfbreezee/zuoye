#include<iostream>
#include<iomanip>
#define N 9               //宏定义九个评委
using namespace std;
int main()
{
       cout<<"请输入九个评委分数： "<<endl;
       double *a;
       a=new double [N];
       for(int i=0;i<N;i++)              //赋值数组
              cin>>a[i];
      double min=a[0];
      double max=a[0];
       for(int i=0;i<N;i++)             //找最大最小值
       {
              if(a[i]<min)
                     min=a[i];
              if(a[i]>max)
                     max=a[i];
       }
       double s=0;
       for(int i=0;i<N;i++)
              s+=a[i];
       double ave=(s-max-min)/(N-2);
       cout<<" 平均分为："<<setiosflags(ios::fixed)<<setprecision(3)<<ave<<endl;     //控制格式输入精度为3
       delete []a;
       return 0;
}
