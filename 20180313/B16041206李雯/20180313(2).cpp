#include<iostream>
using namespace std;
template <class ElemType>
ElemType MaxMin(ElemType a[],int n)
{
	ElemType max=a[0];
	for(int i=1;i<n;i++){
		if(max<a[i])
		    max=a[i];
	}
	a[0]=max;
	ElemType min=a[1];
	for(int i=1;i<n;i++){
		if(min>a[i])
		    min=a[i];
	}
	a[1]=min;
 } 
 int main()
 {
 	int a[]={1,3,2,4,6,5,8,7,9},n=9,sum=0;
 	for(int i=2;i<n;i++){
 		sum+=a[i];
	 }
	 cout<<sum/(n-2);
 	return 0;
 	}
