#include <iostream>

using namespace std;

int add1(int n){
	int sum=0;
	for (int i=1;i<=n;i++){
		sum+=i;
	}
	return sum;
}
int add2(int n){
	int sum=add1(9);
	int a,b;
	for (int i=10;i<=n;i++){
		
		a=i/10;
		b=i%10;
		sum+=(a+b);
	}
	return sum;
}
int add(int n){
	if(n<=0||n>=20){
		return 0;
	}
	int sum=0;
	
	if(n/10==0){
		sum=add1(n);
	}
	else{
		sum=add2(n);
	}
	return sum;
	
}

int main (void){
	
	int n;	
	while(true){	
		cin>>n;	
		cout<<add(n)<<endl;
	}
	
	return 0;
}
