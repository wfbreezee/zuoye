#include<iostream>
using namespace std;
int countNum(int n)    //求出每一个n对应的数位和并返回
{
    int sum=0;
	int count=0;
	int a=n;
	while(a!=0)
	{
		count=a%10;
		sum+=count;
		a=a/10;
	}
	return sum;
}
int addNum(int n)    //通过for循环调用countNum函数求的值相加，得到所有数的数位和的和
{
	int sum=0;
	for(int i=1;i<=n;i++)
        sum+=countNum(i);
	return sum;
}
int main()
{
	int n;
	cout<<"请输入一个整数:\n";
	cin>>n;
	cout<<"\n"<<"数位和为:"<<addNum(n)<<endl;
	return 0;
}
